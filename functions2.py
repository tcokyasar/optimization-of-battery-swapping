import numpy as np
from scipy.spatial import distance as dist
import matplotlib.pyplot as plt
from matplotlib import rc
from PIL import Image
from prettytable import PrettyTable
import sympy as sy
import xlrd
from scipy.stats import norm
plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
plt.rcParams['font.family'] = 'Times New Roman'
rc('text', usetex=True)

### BEGIN DEFINING REQUIRED FUNCTIONS TO CREATE THE PROBLEM ###
def filledcircle(x, y, r, msize,ccolor):
    circle = plt.Circle((x, y), r,color='lightgray',fill=False, ec='none') #,alpha=0.8
    point = plt.plot(x, y, '.',color=ccolor,markersize=msize)
    return plt.gca().add_artist(circle), point
def circleframe(x, y, r, msize): #ABSM locator
    circle = plt.Circle((x, y), r,color='lightgray',fill=False, ec='darkgray')
    point = plt.plot(x, y, 's',color='black',markersize=msize)
    return plt.gca().add_artist(circle),point
def unfilledcirclenodot(x, y, r):
    circle = plt.Circle((x, y), r,color='lightgray',fill=False, ec='darkgray')
    return plt.gca().add_artist(circle)
def unfilleddashedcircle(x, y, r, msize): #FC locator
    circle = plt.Circle((x, y), r,color='lightgray',fill=False, ec='darkgray',alpha = 0.7)
    FC = plt.plot(x, y, '*',color='black',markersize=msize,label='$\mathbf{FC}$')
    return plt.gca().add_artist(circle), FC
def Pixel_finder(px,w,h,BBox,point): #Finds the color of a given pixel
    row = int(((BBox[3]-point[1])/(BBox[3]-BBox[2]))*h)
    col = int(((point[0]-BBox[0])/(BBox[1]-BBox[0]))*w)
    return(px[col,row])
def bBoxPointToColor(px,w,h,BBox,point): #Looks if the given point is on the lake or river
    row = int(((BBox[3]-point[1])/(BBox[3]-BBox[2]))*h)
    col = int(((point[0]-BBox[0])/(BBox[1]-BBox[0]))*w)
    return sum(abs(k[0]-k[1]) for k in zip(px[col,row],(255, 255, 255))) < 10
def distance(origin, destination):
    return(float(dist.cdist([origin], [destination], "euclidean")*49.5))

def ABSM_generator(number, FC_loc,img,px,BBox): #Pass FC loc so it avoids generating ABSM
    xvals = list(np.linspace(BBox[0], BBox[1], int(number**0.5)+2))[1:-1] #ABSM x-axis linspaced
    yvals = list(np.linspace(BBox[2], BBox[3], int(number**0.5)+2))[1:-1] #ABSM y-axis linspaced
    ABSMpairs = [(i,j) for i in xvals for j in yvals if not (
               i == FC_loc[0] and j == FC_loc[1] or
              bBoxPointToColor(px,img.width,img.height,BBox,[i,j]))]
    return(ABSMpairs,xvals,yvals)
### END DEFINING REQUIRED FUNCTIONS TO CREATE THE PROBLEM ###

def problem_plotter(seedno, R_dist, Custnum, ABSMnum,active):
    np.random.seed(seedno)
    img = Image.open('map2.png')
    px = img.load()
    BBox = ((-88.34, -87.54,
             41.72, 42.31))

    sheet0 = xlrd.open_workbook("points2.xlsx").sheet_by_index(0) #Locates the sheet
    xcoor = sheet0.col_values(4,1)                               #Gets customer x
    ycoor = sheet0.col_values(3,1)                               #Gets customer y
    household_pop = [int(i) for i in sheet0.col_values(1,1)]     #Gets hosuehold_pop
    person_pop = [int(i) for i in sheet0.col_values(2,1)]        #Gets person_pop
    for i in range(len(person_pop)):
        if person_pop[i] == 0 or household_pop[i] == 0:
            person_pop[i] = int(np.mean(person_pop))
            household_pop[i] = int(np.mean(household_pop))
            
    FCpairs = ((BBox[0]+BBox[1])/2,(BBox[2]+BBox[3])/2) #FC location
    (Ipairs,xvals,yvals) = ABSM_generator(ABSMnum,
                            FCpairs,img,px,BBox)        #ABSM locations
    
    '''The following selects customers randomly out of the POLARIS pot.'''
    selected_cust = []
    while len(selected_cust) < Custnum:
        cust = np.random.choice(len(xcoor))
        if (distance((xcoor[cust],ycoor[cust]),FCpairs) > R_dist 
            and cust not in selected_cust):
            selected_cust.append(cust)
            
    Dpairs = [(xcoor[i],ycoor[i]) for i in selected_cust]
    pop_selected = [(household_pop[i],person_pop[i]) for i in selected_cust]

    Vpairs = [FCpairs]+Ipairs+Dpairs #Set of vertices with (x,y) locations
    FC = [0]; I = [i for i in range(max(FC)+1,len(Ipairs)+1)]
    D = [i for i in range(max(I)+1,max(I)+len(Dpairs)+1)]
    V = FC + I + D #Set of vertices with enumerated labels
    Vdict = dict(zip(V, Vpairs))


    fig, ax = plt.subplots(figsize = (15,15))           #Adjusts figure size
    ax.set_xlim(BBox[0],BBox[1])                        #Adjusts x-axis to map
    ax.set_ylim(BBox[2],BBox[3])                        #Adjusts y-axis to map
    ax.axis('off')

    c_marker = 12; ABSM_marker = 8; FC_marker = 20     #Marker size settings

    r_scaled = 1/49.5*R_dist                            #Converts R into image scale
    for i in Dpairs:        
        plt.plot(i[0],i[1], '.k',markersize=c_marker)   #Plots Customers
    for i in Ipairs:
        circleframe(i[0],i[1], r_scaled,ABSM_marker)    #Plots ABSMs
    plt.plot(FCpairs[0],FCpairs[1], '.k', alpha =0.5, 
         label = '$R$',markersize=FC_marker,
         markerfacecolor = 'none')                      #Add R label
    unfilleddashedcircle(FCpairs[0],FCpairs[1],
         r_scaled,FC_marker)                            #Plot FC and add label
    plt.plot(Ipairs[0][0],Ipairs[0][1], 'sk',           #Add ABSM label
             label = '$\mathbf{ABSM}$',
             markersize=ABSM_marker)
    plt.plot(Dpairs[0][0],Dpairs[0][1], '.k',           #Add Customer label
             label = '$\mathbf{Customer}$',
             markersize=c_marker)
    if active == True:
        for i in Vdict:
            plt.text(Vdict[i][0]*0.99999,Vdict[i][1]*1.0003,r'$\mathbf{%s}$'%(i),size=15)
            
    plt.gca().axes.get_xaxis().set_visible(False)       #Disables x axis
    plt.gca().axes.get_yaxis().set_visible(False)       #Disables y axis

    plt.legend(loc='upper right', fancybox=True,        #Adds legend box
               shadow=False, ncol=1, numpoints=1, 
               fontsize = 20)
    plt.imshow(img, zorder=0, extent = BBox,            #Shows the figure
              aspect= 'equal')
    plt.savefig('problem_figure.png', dpi=300, bbox_inches = 'tight',pad_inches = 0)
    plt.show()
    return(FCpairs,Ipairs,Dpairs,Vpairs,FC,I,D,V,Vdict,pop_selected)

