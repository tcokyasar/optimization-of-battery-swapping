from gurobipy import *
from datetime import datetime
from functions2 import *

def Solver(R,R_dist,speed,FC,I,D,V,Cphi,Ctheta,Cdelta,Ctau,Cbeta,S,T,P,
           Tdelta,Dtau,lmbda,mu,timelimit,desired_gap,seedno,
           FCpairs,Ipairs,Dpairs,Vpairs,Vdict,labels):
    ### BEGIN CREATING THE SOLUTION PLOTTER ###
    def solution_plotter(R_dist,active,iteration):
        img = Image.open('map2.png')
        px = img.load()
        BBox = ((-88.34, -87.54,
             41.72, 42.31))

        fig, ax = plt.subplots(figsize = (15,15))           #Adjusts figure size
        ax.set_xlim(BBox[0],BBox[1])                        #Adjusts x-axis to map
        ax.set_ylim(BBox[2],BBox[3])                        #Adjusts y-axis to map
        ax.axis('off')

        c_marker = 7; ABSM_marker = 8; FC_marker = 20     #Marker size settings
        r_scaled = 1/49.5*R_dist                            #Converts R into image scale

        plt.plot(FCpairs[0],FCpairs[1], '.k', alpha =0.5,   #Add R label
             label = '$R$',markersize=FC_marker,
             markerfacecolor = 'none')       
        unfilleddashedcircle(FCpairs[0],FCpairs[1],
             r_scaled,FC_marker)                            #Plot FC and add label
        
        if sum([x[j].x for j in I])>0:
            Firstopen = [j for j in I if x[j].x>0][0]-1
            plt.plot(Ipairs[Firstopen][0],Ipairs[Firstopen][1], 's',  #Add ABSM label for Open
                     color = 'black',
                     label = '$\mathbf{Selected~ABSM}$',
                     markersize=ABSM_marker)
        if sum([x[j].x for j in I])<len(I):
            Firstclosed = [j for j in I if x[j].x==0][0]-1
            plt.plot(Ipairs[Firstclosed][0],Ipairs[Firstclosed][1], 's',   #Add ABSM label for Closed
                     color = 'black', markerfacecolor='none',
                     label = '$\mathbf{Rejected~ABSM}$',
                     markersize=ABSM_marker)
        if max([r[d].x for d in D])>0.999:
            Firstdrone = [d for d in D if r[d].x>0.9999][0]-len(Ipairs)-1
            plt.plot(Dpairs[Firstdrone][0],Dpairs[Firstdrone][1], 'P', #Add Drone Delivery label
                     color = "black",
                     label = '$\mathbf{Drone~Delivery}$',
                     markersize=c_marker)
        if sum([z[d].x for d in D])>0:
            Firsttruck = [d for d in D if z[d].x>0.9999][0]-len(Ipairs)-1
            plt.plot(Dpairs[Firsttruck][0],Dpairs[Firsttruck][1], 'p',        #Add Truck Delivery label
                     color = "black",
                     label = '$\mathbf{Truck~Delivery}$',
                     markersize=c_marker)
        if "Yes" in ["Yes" for d in D if (r[d].x>0 and z[d].x>0)]:
            Firstmixed = [d for d in D if z[d].x>0 and r[d].x>0][0]-len(Ipairs)-1
            plt.plot(Dpairs[Firstmixed][0],Dpairs[Firstmixed][1], '^',        #Add Mixed Delivery label
                     color = "black",
                     label = '$\mathbf{Mixed~Delivery}$',
                     markersize=c_marker)
        if active == True:                                  #Add point labels if True
            for i in Vdict:
                plt.text(Vdict[i][0]*0.99999,
                         Vdict[i][1]*1.0003,
                         r'$\mathbf{%s}$'%(i),size=15)
        for j in I:
            if x[j].x > 0:
                plt.plot(Vdict[j][0], Vdict[j][1], 's',     #Plot open ABSMs
                    color='black',
                    markersize=ABSM_marker)
                filledcircle(Vdict[j][0],                   #Plot open ABSM circles
                    Vdict[j][1], r_scaled,
                    ABSM_marker,'black')
        for j in I:
            if x[j].x > 0.1:
                unfilledcirclenodot(Vdict[j][0],            #Plot open circles
                    Vdict[j][1], r_scaled)
            else:
                plt.plot(Vdict[j][0], Vdict[j][1], 's',     #Plots closed ABSMs
                    color='black',markerfacecolor='none',
                         markersize=ABSM_marker)

        for d in D:                                         
            if r[d].x > 0 and z[d].x < 0.000001 and sum(y[i,d,d].x for i in I if Tdelta[i,d] <= R)>0.5:
                plt.plot(Vdict[d][0],Vdict[d][1],           #Plots Drone Delivery  
                         'P',color = "black",
                         markersize=c_marker)
            if z[d].x > 0 and r[d].x < 0.000001 and sum(y[i,d,d].x for i in I if Tdelta[i,d] <= R)<0.5:
                plt.plot(Vdict[d][0],Vdict[d][1],           #Plots Truck Delivery
                         'p',color = "black",
                         markersize=c_marker)
            if r[d].x > 0 and z[d].x > 0 and sum(y[i,d,d].x for i in I if Tdelta[i,d] <= R)>0.5:                   
                plt.plot(Vdict[d][0],Vdict[d][1],           #Plots Mixed Delivery
                         '^',color = "black",
                         markersize=c_marker)

        plt.gca().axes.get_xaxis().set_visible(False)       #Disables x axis
        plt.gca().axes.get_yaxis().set_visible(False)       #Disables y axis

        plt.legend(loc='upper right', fancybox=True,        #Adds legend box
                   shadow=False, ncol=1, numpoints=1, 
                   fontsize = 20)
        plt.imshow(img, zorder=0, extent = BBox,            #Shows the figure
                  aspect= 'equal')
        plt.savefig('progress_iteration_{}.png'.format(iteration), dpi=300,
                    bbox_inches = 'tight',pad_inches = 0)
        plt.show()
    ### END CREATING THE SOLUTION PLOTTER ###

    ### BEGIN SOLUTION PRINTER ###
    def solutionprinter():
        print('\x1b[1;23;42m' +'Open ABSM Locations:'+ '\x1b[0m')
        for j in I:
            if x[j].x > 0.1:
                print("ABSM {} is open".format(j))
        print("\n")
        print('\x1b[1;23;42m' +'The transport mode for each d:'+ '\x1b[0m')
        for d in D:
            if z[d].x >0.:
                print("{} percent demand of vertex {} is served by trucks.".format(round(z[d].x*100,2),d))
            if r[d].x >0:
                print("{} percent demand of vertex {} is served by drones.".format(round(r[d].x*100,2),d))
        for j in I:
            if L[j].x > 0.:
                print("Average number of drones at ABSM vertex {} is {}.".format(j,round(L[j].x,4)))
            if b[j].x > 0.:
                print("Minimum number of battery stock at ABSM vertex {} is {}.".format(j,round(b[j].x,4)))
        if w.x > 0.:
            print("Average number of drones neded is {}.".format(round(w.x,3)))
        print('\n\x1b[1;23;43m' +"The LB objective value is $%s"% str(m.ObjBound) + '\x1b[0m\n')
        print('\n\x1b[1;23;44m' +'The UB objective value is $%s'%str(UB_calculator()) +'\x1b[0m\n')
        print ('\n\x1b[1;23;45m' + 'The solution gap is currently %s'
               %(1-m.ObjBound/UB_calculator()) + '\x1b[0m\n')
        return("")
    ### END SOLUTION PRINTER ###

    ### BEGIN Lq CALCULATOR ###
    def Lcalc(lmbdaval): #L calculator given lmbdaval
        return(((lmbdaval**2/mu**2 + 2*P*lmbdaval**2/(T*mu) + 2*P*lmbdaval**2/T**2)/(2*(1-lmbdaval/mu-P*lmbdaval/T))
                + (lmbdaval*(mu*P+mu*T*P+T**2+T*P))/(T**2*mu*(1+P/T))))
    ### END Lq CALCULATOR ###

    ### BEGIN UPPER BOUND C_HAT CALCULATOR ###
    def UB_calculator(): #Upper bound calculator
        C_hat = (sum(Cphi[j]*x[j].x for j in I)                                          #ABSM Cost
                 +sum(2*lmbda[d]*Cdelta*Tdelta[i,j]*g[i,j,d].x for (i,j) in E for d in D)#Drone delivery cost
                 +sum(lmbda[d]*Ctau*Dtau[d]*z[d].x for d in D)                           #Truck delivery cost
                 +Ctheta*w.x                                                             #Drone ownership cost
                 +sum(Cbeta*b[j].x for j in I)                                           #Battery cost
                 +sum(Cdelta*Lcalc(lmbdavar[j].x) for j in I))                           #Drone congestion cost
        return(C_hat)
    ### END UPPER BOUND C_HAT CALCULATOR ###

    ### BEGIN CALCULATING SOLUTION METRICS ###
    def solution_metrics_calculator():
        Total_ABSMs_used = sum([1 for j in I if x[j].x>0])
        Num_demand_assigned_to_drones = sum([lmbda[d]*r[d].x for d in D])
        Num_demand_assigned_to_trucks = sum([lmbda[d]*z[d].x for d in D])
        lmbda_list = [lmbdavar[j].x for j in I if lmbdavar[j].x>0]
        L_list = [L[j].x for j in I if L[j].x>0]
        Avg_lmbda_j = (round(np.mean(lmbda_list),5) if len(lmbda_list)>0 else 0)
        Avg_L_j = (round(np.mean(L_list),5) if len(L_list)>0 else 0)
        C = m.ObjBound
        C_hat = UB_calculator()
        global solution_gap; solution_gap = 1-(C/C_hat)
        solution_time = (datetime.now()-starttime).total_seconds()
        Cor_avg_L_j = np.mean([Lcalc(lmbdavar[j].x) for j in I])
        return(Total_ABSMs_used, Num_demand_assigned_to_drones, 
               Num_demand_assigned_to_trucks, Avg_lmbda_j, Cor_avg_L_j, C, C_hat,
               solution_gap, solution_time)
    ### END CALCULATING SOLUTION METRICS ###

    ### BEGIN PRINTING SOLUTION METRICS ###
    def table_printer():
        (Total_ABSMs_used, Num_demand_assigned_to_drones, 
               Num_demand_assigned_to_trucks, Avg_lmbda_j, Cor_avg_L_j, C, C_hat,
               solution_gap, solution_time) = solution_metrics_calculator()
        table = PrettyTable(['seed', '# d by Drones', '# d by Trucks', '# ABSMs',
                             'Avg. Lambda', 'Avg. L', 
                             'C', 'C_hat', 'Gap', 'Time'])
        table.add_row([seedno, Num_demand_assigned_to_drones if Num_demand_assigned_to_drones>0 else "-",
                       Num_demand_assigned_to_trucks if Num_demand_assigned_to_trucks>0 else "-",
                       Total_ABSMs_used if Total_ABSMs_used>0 else "-",
                       round(Avg_lmbda_j,5) if Avg_lmbda_j>0 else "-",
                       round(Cor_avg_L_j,7) if Cor_avg_L_j>0 else "-",
                       '{:,.1f}'.format(round(C,2)), '{:,.1f}'.format(round(C_hat,2)),
                       round(solution_gap,6), round(solution_time,2)])
        print(table); print('\n\n')
    ### END PRINTING SOLUTION METRICS ###

    ### BEGIN (A,B) CALCULATOR ###
    def ab_calc(lmbdaval):
        lmbdasy = sy.symbols('lambdasy', real=True)
        L = (((lmbdasy**2/mu**2 + 2*P*lmbdasy**2/(T*mu) + 2*P*lmbdasy**2/T**2)/(2*(1-lmbdasy/mu-P*lmbdasy/T))
                + (lmbdasy*(mu*P+mu*T*P+T**2+T*P))/(T**2*mu*(1+P/T))))
        d_L = sy.diff(L, lmbdasy).doit()
        B = (d_L.subs(lmbdasy, lmbdaval))
        A = (L.subs(lmbdasy, lmbdaval) - d_L.subs(lmbdasy, lmbdaval)*lmbdaval)
        return(A,B)
    ### END (A,B) CALCULATOR ###

    ### BEGIN DEFINING CUT CONSTRAINT GENERATOR ###
    def cut_cons_gen():
        cut_dict = {j: ab_calc(lmbdavar[j].x) for j in I if lmbdavar[j].x>0}
        for j in cut_dict.keys():
            for j_prime in I:
                m.addConstr(L[j_prime] >= cut_dict[j][0] + cut_dict[j][1]*lmbdavar[j_prime] 
                        - M3*(1-x[j_prime]), name = "cut_cons{}".format(j,j_prime))
#                 print("I have just added a new cut " +
#                       "based on a+b(lmbda)=%s+%s(lmbda) to location %s."%(cut_dict[j][0],
#                         cut_dict[j][1],j_prime))
        print("\nAt this iteration, we introduced %s cuts in total.\n"%(len(I)*len(cut_dict.keys())))
        return(print("Now beginning to solve the new program with added cuts!\n"))
    ### END DEFINING CUT CONSTRAINT GENERATOR ###

    def subI(set1,v2,R): #Subset of given sets
        return([v1 for v1 in set1             
                if(v1!=v2 and Tdelta[v1,v2]<=R)])
    E = [(i,j) for i in V for j in V          #Set of edges
         if(i!=j and Tdelta[i,j]<=R and i not in D and j not in FC)]
    M1 = 100                                              #Big M1
    M2 = 2*max([len(subI(FC+I,j,R)) for j in I])*len(D)   #Big M2 Times 2 is for guarantee
    M3 = max(Lcalc(sum(2*lmbda[d] for d in D)),Lcalc(S*T*mu/(T+P*mu))) #Big M3 Times 2 is for guarantee
    ### END DEFINING PARAMETERS ###

    ### BEGIN CREATING THE MODEL ###
    m = Model("Drone-delivery-problem")
    starttime = datetime.now()
    ### END CREATING THE MODEL ###

    ### BEGIN INTRODUCING VARIABLES ###
    x, y, z, r, lmbdavar, L, w, b = {}, {}, {}, {}, {}, {}, {}, {}
    e, g = {}, {}
    for j in I:
        x[j] = m.addVar(vtype=GRB.BINARY, name = "x%s"%str([j]))
        lmbdavar[j] = m.addVar(vtype=GRB.CONTINUOUS, name="lmbdavar%s"%str([j]))
        L[j] = m.addVar(vtype=GRB.CONTINUOUS, name="L%s"%str([j]))
        b[j] = m.addVar(vtype=GRB.CONTINUOUS, name="b%s"%str([j]))
        e[j] = m.addVar(vtype=GRB.CONTINUOUS, name="e%s"%str([j]))
    for d in D:
        z[d] = m.addVar(vtype=GRB.CONTINUOUS, name = "z%s"%str([d]))
        r[d] = m.addVar(vtype=GRB.CONTINUOUS, name = "r%s"%str([d]))
        for (i,j) in E:
            y[i,j,d] = m.addVar(vtype=GRB.BINARY, name = "y%s"%str([i,j,d]))
            g[i,j,d] = m.addVar(vtype=GRB.CONTINUOUS, name = "g%s"%str([i,j,d]))
    w = m.addVar(vtype=GRB.CONTINUOUS, name="w")
    ### END INTRODUCING VARIABLES ###

    ### BEGIN CREATING THE OBJECTIVE FUNCTION ###
    m.setObjective((quicksum(Cphi[j]*x[j] for j in I)                                            #ABSM Cost
                    +quicksum(2*lmbda[d]*Cdelta*Tdelta[i,j]*g[i,j,d] for (i,j) in E for d in D)  #Drone delivery cost
                    +quicksum(lmbda[d]*Ctau*Dtau[d]*z[d] for d in D)                             #Truck delivery cost
                    +Ctheta*w                                                                    #Drone ownership cost
                    +quicksum(Cbeta*b[j] for j in I)                                             #Battery cost
                    +quicksum(Cdelta*L[j] for j in I)),                                          #Drone congestion cost
                     GRB.MINIMIZE)
    ### END CREATING THE OBJECTIVE FUNCTION ###

    ### BEGIN CREATING THE CONSTRAINTS ###
    for d in D:
        m.addConstr(r[d] + z[d] == 1, name = "demand_must_be_met{}".format(d))
        m.addConstr(r[d] <= quicksum(y[i,d,d] for i in I if Tdelta[i,d] <= R), 
                    name = "drone_must_fly_if_ratio_is_not_zero1{}".format([d]))
        m.addConstr(quicksum(y[i,d,d] for i in I if Tdelta[i,d] <= R) <= M1*r[d], 
                    name = "drone_must_fly_if_ratio_is_not_zero2{}".format([d]))
        for i in I:
            if Tdelta[i,d] <= R:
                m.addConstr(2*Tdelta[i,d]*y[i,d,d] <= R, name = "drone_must_have_enough_battery{}".format([d]))
        for j in I:
            m.addConstr(quicksum(y[i,j,d] for i in subI(FC+I,j,R)) ==
                        quicksum(y[j,i,d] for i in subI(I+D,j,R)), 
                        name = "drone_in_vertex_drone_out_vertex{}".format([j,d]))
        for (i,j) in E:
            m.addConstr(g[i,j,d] <= y[i,j,d], name = "linearizing_lambda_j1{}".format([i,j,d]))
            m.addConstr(r[d]+y[i,j,d]-1 <= g[i,j,d], name = "linearizing_lambda_j2{}".format([i,j,d]))
            m.addConstr(g[i,j,d] <= r[d], name = "linearizing_lambda_j3".format([i,j,d]))
            
    for j in I:
        m.addConstr(quicksum(y[i,j,d] for i in subI(FC+I,j,R) for d in D) <= M2*x[j],
                    name = "drone_can_use_route_if_ABSM_open{}".format([j]))
        m.addConstr(lmbdavar[j] <= S*T*mu/(T+P*mu), name = "stability{}".format([j]))
        m.addConstr(lmbdavar[j] == quicksum(lmbda[d]*g[i,j,d] for i in subI(FC+I,j,R) for d in D),
                    name = "linear_lambda_j{}".format([j]))
        m.addConstr(b[j] >= T*lmbdavar[j] + norm.ppf(1-P)*(T**(1/2))*e[j], 
                    name = "linear_number_of_batteries_needed1{}".format([j]))
        m.addQConstr(e[j]*e[j] == lmbdavar[j], name = "Quadratic{}".format([j]))
    m.addConstr(w >= quicksum(Tdelta[i,j]*lmbdavar[j] for (i,j) in E if j not in D) + quicksum(L[j] for j in I),
                name = "number_of_drones_needed")
    ### END CREATING THE CONSTRAINTS ###

    ### BEGIN SOLVING ###
    print("Beginning to solve the problem!\n")
    m.setParam('TimeLimit', timelimit); #m.setParam('MIPgap', 0.000001)
    m.setParam('NonConvex', 2)
    m.update(); m.optimize(); print("\n\n")
    
    
#     m.write("test.lp") # Writes the model in mathematical form (very important for detection)
#     if m.status == GRB.INFEASIBLE: # If infeasibility exists, it does following:
#         m.computeIIS()  # Computes Irreducible Incosistent Subsytems (Not Vital)
#         m.feasRelaxS(1, False, False, True) # Relaxes the problem so that it becomes feasible
#         m.optimize() # Give the optimize command once again!
#         m.printAttr('X', 'Art*') # Shows where the relaxation is done, so the problem can be resolved
    
    Remainingtime = timelimit-(datetime.now()-starttime).total_seconds()
    table_printer(); solution_plotter(R_dist,labels,0)
    LB, UB = {}, {}
    LB[0] = m.ObjBound; UB[0] = UB_calculator()
    ### END SOLVING ###

    ### BEGIN ADDING CUTS AND SOLVING ITERATIVELY ###
    cut_iter_gap =[solution_gap]; iteration = 1
    while solution_gap > desired_gap and Remainingtime > 0:
        starttimer = datetime.now()
        cut_cons_gen(); m.setParam('TimeLimit', Remainingtime)
        m.update; m.optimize(); print("\n\n")
        table_printer(); print(solutionprinter())
        solution_plotter(R_dist,labels,iteration)
        cut_iter_gap.append(solution_gap)
        Remainingtime -= (datetime.now()-starttimer).total_seconds()
        LB[iteration] = m.ObjBound; UB[iteration] = UB_calculator()
        iteration += 1
        if cut_iter_gap[-1] == cut_iter_gap[-2]:
            break
    ### END ADDING CUTS AND SOLVING ITERATIVELY ###
    xsol = {j:x[j].x for j in I}
    ysol = {(i,j,d):y[i,j,d].x for (i,j) in E for d in D}
    gsol = {(i,j,d):g[i,j,d].x for (i,j) in E for d in D}
    zsol = {d: z[d].x for d in D}
    rsol = {d: r[d].x for d in D}
    lmbdavarsol = {j: lmbdavar[j].x for j in I}
    Lsol = {j: L[j].x for j in I}
    wsol = w.x
    bsol = {j: b[j].x for j in I}
    esol = {j: e[j].x for j in I}
    comptime = timelimit-Remainingtime
    return(xsol,ysol,gsol,zsol,rsol,lmbdavarsol,Lsol,wsol,bsol,esol,LB,UB,comptime)